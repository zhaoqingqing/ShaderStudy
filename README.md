### 前言

《UnityShader 入门精要》 练习

作者github：https://github.com/candycat1992/Unity_Shaders_Book

### 开发环境

windows7/10 x64

visual studio 2013/2015 + shaderlabVS

unity 5.3.6f1 x64

### UnityShader Book资源

《Unity Shader入门精要》勘误

http://candycat1992.github.io/unity_shaders_book/unity_shaders_book_corrigenda.html



###unity shader代码高亮、部分提示

国人自制visual studio 的unity shader提示扩展：ShaderlabVS

https://github.com/wudixiaop/ShaderlabVS



另一个支持visual studio的扩展：Shader Unity Support

https://marketplace.visualstudio.com/items?itemName=MarcinODev.ShaderUnitySupport



不建议两个同时使用，可能会失效，优先使用ShaderlabVS