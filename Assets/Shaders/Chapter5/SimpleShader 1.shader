﻿Shader "Study/Chapter5/SimpleShader 1"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			//在无光照的条件下，会渲染出一个纯白色
			#pragma vertex vert
			#pragma fragment frag

			struct a2v{
				float4 vertex:POSITION;
				float3 normal:NORMAL;
				float4 texcoord:TEXCOORD;
			};//语法严格，如果少了个;也会报错
			
			//顶点着色器
			float4 vert (a2v v) :SV_POSITION
			{
				return mul(UNITY_MATRIX_MVP,v.vertex);
			}
			
			//片段着色器
			fixed4 frag () : SV_Target
			{
				//表面的颜色0~1 黑色到白色
				//return fixed4(0.1,0.1,0.1,1.0);
				return fixed4(1,1,1,1.0);
			}
			ENDCG
		}
	}
}
