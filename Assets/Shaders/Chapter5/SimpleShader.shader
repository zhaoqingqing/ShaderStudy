﻿Shader "Study/Chapter5/SimpleShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			//在无光照的条件下，会渲染出一个纯白色
			#pragma vertex vert
			#pragma fragment frag
	
			float4 vert (float4 v : POSITION) :SV_POSITION
			{
				return mul(UNITY_MATRIX_MVP,v);
			}
			
			fixed4 frag () : SV_Target
			{
				//表面的颜色0~1 黑色到白色
				//return fixed4(0.1,0.1,0.1,1.0);
				return fixed4(1,1,1,1.0);
			}
			ENDCG
		}
	}
}
