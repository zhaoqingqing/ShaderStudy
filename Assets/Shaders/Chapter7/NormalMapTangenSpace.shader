﻿Shader "Study/Chapter7/NormalMapTangenSpace"
{
	//纹理的高光照
	Properties
	{
		_Color("Color Tint",Color) = (1,1,1,1)		
		_MainTex("Main Text",2D) = "white" {}
		_BumpMap("Normal Map",2D) = "bump" {}
		_BumpScale("Bump Scale",float) = 1.0
		_Specular("Specular",Color) = (1,1,1,1)
		_Gloss("Gloss",Range(8.0,256)) = 20
	}
	SubShader
	{
		Pass
		{
			Tags { "LightMode"="ForwardBase" }
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "Lighting.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _BumpMap;
			float4 _BumpScale_ST;//得到纹理的平铺和偏移系数
			float _BumpScale;
			fixed4 _Specular;
			float _Gloss;
			//顶点输入结构
			struct a2v
			{
				float4 vertex:POSITION;
				float3 normal:NORMAL;
				float4 tangent:tangent;
				float4 texcoord:TEXCOORD;
			};

			//vertex2frag?
			struct v2f
			{
				float4 pos:SV_POSITION;
				float4 uv:TEXCOORD;
				fixed3 lightDir:TEXCOORD1;
				fixed3 viewDir:TEXCOORD2;
			};
			
			//顶点
			v2f vert(a2v v)
			{
				v2f o;	
				o.pos = mul(UNITY_MATRIX_MVP,v.vertex);
				o.uv.xy = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
				o.uv.zw = v.texcoord.xy * _BumpScale_ST.xy + _BumpScale_ST.zw;
				
				float3 binormal = cross(normalize(v.normal),normalize(v.tangent.xyz)) * v.tangent.w;
				float3x3 rotation = float3x3(v.tangent.xyz,binormal,v.normal);
				
				o.lightDir = mul(rotation,ObjSpaceLightDir(v.vertex)).xyz;
				o.viewDir = mul (rotation,ObjSpaceViewDir(v.vertex)).xyz;

				return o;
			}
		
			float4 frag (v2f i) : SV_Target
			{
				fixed3 tangentLightDir = normalize(i.lightDir);
				fixed3 tangentViewDir = normalize(i.viewDir);
				
				fixed4 packedNormal = tex2D(_BumpMap,i.uv.zw);
				fixed3 tangentNormal;
				tangentNormal = UnpackNormal(packedNormal);
				tangentNormal.xyz *= _BumpScale;
				tangentNormal.z = sqrt(1.0 - saturate(dot(tangentNormal.xy,tangentNormal.xy)));

				fixed3 albedo = tex2D(_MainTex,i.uv).rgb * _Color.rgb;
				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz * albedo;
				fixed3 diffuse = _LightColor0.rgb * albedo * max(0,dot( tangentNormal,tangentLightDir));
				
				fixed3 halfDir = normalize(tangentLightDir + tangentViewDir);
				fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(max(0,dot(tangentNormal,halfDir)),_Gloss);	
				fixed3 fragColor = albedo + diffuse + specular;	
				return fixed4(fragColor,1.0);
			}
			ENDCG
		}
	}

	FallBack "Specular"
}
