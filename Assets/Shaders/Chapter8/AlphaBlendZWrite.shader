﻿Shader "Study/Chapter8/AlphaBlendZWrite"
{
	//透明度测试
	Properties
	{
		_Color("Color Tint",Color) = (1,1,1,1)		
		_MainTex("Main Text",2D) = "white" {}
		_AlphaScale("Alpha Scale",Range(0,1)) = 1
	}
	SubShader
	{
		//透明度通常需要指定以下三个标签
		Tags
		{	"Queue"="Transparent" 
			"IgnoreProjector"="true"
			"RenderType"="Transparent"	
		}
		Pass
		{
			//这个Pass开启ZWrite，
			ZWrite On
			ColorMask 0		//并且不写入颜色		
		}
		Pass
		{
			Tags{"LightMode"="ForwardBase"}	
			ZWrite Off	//关闭了ZWrite
			//混合因子，半透明效果
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "Lighting.cginc"
			
			fixed4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed _AlphaScale;
			
			struct a2v
			{
				float4 vertex:POSITION;
				float3 normal:NORMAL;
				float4 texcoord:TEXCOORD;
			};

			struct v2f
			{
				float4 pos:SV_POSITION;
				fixed3 worldNormal:TEXCOORD;
				fixed3 worldPos:TEXCOORD1;
				fixed2 uv:TEXCOORD2;
			};
			
			//顶点
			v2f vert(a2v v)
			{
				v2f o;	
				o.pos = mul(UNITY_MATRIX_MVP,v.vertex);

				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.worldPos = mul(_Object2World,v.vertex).xyz;
				o.uv = TRANSFORM_TEX(v.texcoord,_MainTex);
				
				return o;
			}
			
			float4 frag (v2f i) : SV_Target
			{
				fixed3 worldNormal = normalize(i.worldNormal);
				fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
				
				fixed4 texColor = tex2D(_MainTex,i.uv);

				fixed3 albedo= texColor.rgb * _Color.rgb;
				fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.xyz *albedo;
				fixed3 diffuse = _LightColor0.rgb *albedo *max(0,dot(worldNormal,worldLightDir)); 
				

				//返回顶点颜色
				fixed3 vertexColor = ambient + diffuse ;
				return fixed4(vertexColor,texColor.a *_AlphaScale);
			}
			ENDCG
		}
	}
	
	FallBack "Diffuse"
}
