﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 边缘检测
/// </summary>
public class EdgeDetection : PostEffectsBase
{
    public Shader edgeDetectShader;
    private Material edgeDetectMaterial;
    [Range(0.0f,1.0f)]
    public float edgesOnly = 0.0f;
    //边缘颜色
    public Color EdgeColor = Color.black;
    //背景颜色
    public Color BackgroundColor = Color.white;

    public Material Material
    {
        get
        {
            edgeDetectMaterial = CheckShaderAndCreateMaterial(edgeDetectShader, edgeDetectMaterial);
            return edgeDetectMaterial;
        }
    }

    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (Material != null)
        {
            Material.SetFloat("_EdgeOnly", edgesOnly);
            Material.SetColor("_EdgeColor", EdgeColor);
            Material.SetColor("_BackgroundColor", BackgroundColor);

            Graphics.Blit(source, destination, Material);
        }
        else
        {
            Graphics.Blit(source,destination);
        }
    }



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
